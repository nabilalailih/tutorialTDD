from django.test import TestCase
from django.test import Client
from django.urls import resolve
# Create your tests here.

class Lab7UnitTest(TestCase):

    def test_lab_7_url_is_exist(self):
        response = Client().get('/lab-7/')
        self.assertEqual(response.status_code, 200)
