// FB initiation function
  window.fbAsyncInit = () => {
    FB.init({
      appId      : '547706468895562',
      cookie     : true,
      xfbml      : true,
      version    : 'v2.11',
    });

    // implementasilah sebuah fungsi yang melakukan cek status login (getLoginStatus)
    // dan jalankanlah fungsi render di bawah, dengan parameter true jika
    // status login terkoneksi (connected)

    // Hal ini dilakukan agar ketika web dibuka dan ternyata sudah login, maka secara
    // otomatis akan ditampilkan view sudah login
  };

  // Call init facebook. default dari facebook
  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "https://connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));

  // Fungsi Render, menerima parameter loginFlag yang menentukan apakah harus
  // merender atau membuat tampilan html untuk yang sudah login atau belum
  // Ubah metode ini seperlunya jika kalian perlu mengganti tampilan dengan memberi
  // Class-Class Bootstrap atau CSS yang anda implementasi sendiri
  
  const render = loginFlag => {
    if (loginFlag) {
      // Jika yang akan dirender adalah tampilan sudah login

      // Memanggil method getUserData (lihat ke bawah) yang Anda implementasi dengan fungsi callback
      // yang menerima object user sebagai parameter.
      // Object user ini merupakan object hasil response dari pemanggilan API Facebook.
      getUserData(user => {
        // Render tampilan profil, form input post, tombol post status, dan tombol logout
        $('#lab8').html(
          '<div class="profile">' +
            '<img class="cover" src="' + user.cover.source + '" alt="cover" />' +
            '<img class="picture" src="' + user.picture.data.url + '" alt="profpic" />' +
            '<div class="data">' +
              '<h1>' + user.name + '</h1>' +
              '<h2>' + user.about + '</h2>' +
              '<h3>' + user.email + ' - ' + user.gender + '</h3>' +
            '</div>' +
          '</div>' +
          '<input id="postInput" type="text" class="post" placeholder="Ketik Status Anda" />' +
          '<button class="postStatus" onclick="postStatus()">Post ke Facebook</button>' +
          '<button class="logout" onclick="facebookLogout()">Logout</button>'
        );

        // Setelah merender tampilan di atas, dapatkan data home feed dari akun yang login
        // dengan memanggil method getUserFeed yang kalian implementasi sendiri.
        // Method itu harus menerima parameter berupa fungsi callback, dimana fungsi callback
        // ini akan menerima parameter object feed yang merupakan response dari pemanggilan API Facebook
getUserFeed(feed => {
          feed.data.map(value => {
            // Render feed, kustomisasi sesuai kebutuhan.
            var waktu = date(value)
            var pk = getId(value.id)
            if (value.message && value.story) {
              $('.wrapper-feed').append(
                '<div class="feed" id='+pk[1]+'>' +
                  '<h1>' + value.message + '</h1>' +
                  '<h2>' + value.story + '</h2>' +
                  '<h5>' + waktu + '</h5>' +
                  
                '</div>'
              );
            } else if (value.message) {
              $('.wrapper-feed').append(
                '<div class="feed" id='+pk[1]+'>' +
                  '<h1>' + value.message + '</h1>' +
                  '<h5>' + waktu + '</h5>' +
                  
                '</div>'
              );
            } else if (value.story) {
              $('.wrapper-feed').append(
                '<div class="feed" id='+pk[1]+'>' +
                  '<h2>' + value.story + '</h2>' +
                  '<h5>' + waktu + '</h5>' +
                  
                '</div>'
              );
            }
          });
        });
      });
    } else {
      // Tampilan ketika belum login
      // $('#lab8').html('<button class="login" onclick="facebookLogin()">Login</button>');
    }
  };


  function facebookLogin(){
    FB.login(function(response) {
    if (response.authResponse) {
     console.log('Welcome!  Fetching your information.... ');
     render(true);
   }
   else {
    console.log('Tidak bisa login');
   }
  },{scope:'user_about_me, user_birthday, public_profile, user_posts, publish_actions'})
  };


  const facebookLogout = () => {
    // TODO: Implement Method Ini
    // Pastikan method memiliki callback yang akan memanggil fungsi render tampilan belum login
    // ketika logout sukses. Anda dapat memodifikasi fungsi facebookLogout di atas.
    FB.getLoginStatus(function(response) {
        if (response && response.status === 'connected') {
            FB.logout(function(response) {
                document.location.reload();
            });
        }
    });
  };

  // TODO: Lengkapi Method Ini
  // Method ini memodifikasi method getUserData di atas yang menerima fungsi callback bernama fun
  // lalu merequest data user dari akun yang sedang login dengan semua fields yang dibutuhkan di 
  // method render, dan memanggil fungsi callback tersebut setelah selesai melakukan request dan 
  // meneruskan response yang didapat ke fungsi callback tersebut
  // Apakah yang dimaksud dengan fungsi callback?
  const getUserData = () => {
    var x = document.getElementById("profile_data");
    if (x.style.display === "none") {
        x.style.display = "block";
    }
    
    FB.getLoginStatus(function(response){
      if(response.status === 'connected'){
    FB.api('/me?fields=name,first_name,last_name, picture.width(200).height(200), age_range, id, gender, email', 'GET', function (response){
      console.log(response);
      document.getElementById('firstname').innerHTML = response.first_name;
      document.getElementById('lastname').innerHTML = response.last_name;
      document.getElementById('name').innerHTML = response.name;
      document.getElementById('gender').innerHTML = response.gender;
      document.getElementById('age').innerHTML = JSON.stringify(response.age_range['min']);
      
    });
      }
      else{
        console.log('not login apa kek');
      }
      
    });
  
  };

const getUserFeed = (feed) => {
    // TODO: Implement Method Ini
    // Pastikan method ini menerima parameter berupa fungsi callback, lalu merequest data Home Feed dari akun
    // yang sedang login dengan semua fields yang dibutuhkan di method render, dan memanggil fungsi callback
    // tersebut setelah selesai melakukan request dan meneruskan response yang didapat ke fungsi callback
    // tersebut
  // FB.getLoginStatus(function(response) {
  //       if (response.status === 'connected') {
  //           FB.api('/me/feed', 'GET', function(response){
  //             console.log(response.message);
  //             console.log(response.data);
  //             feed && feed(response);
  //             console.log('udah lewatin &&');
  //           });
  //       }
  //   });
  // };

  FB.api(
    "/me/feed",
    function (response) {
      if (response && !response.error) {
        console.log(JSON.parse(response));
      }
    }
);
}

  function populateFacebookUpdates() {

            FB.api(
            "/me/feed",
            function (response) {
            if (response && !response.error) {
              console.log(response.data[0].message);
              document.getElementById('latest').innerHTML = response.data[0].message;
              document.getElementById('latest_time').innerHTML = 'Posted on '+ response.data[0].created_time.substring(0,10) + ' at ' + 
              response.data[0].created_time.substring(11,19);
              // /"2017-12-01T18:47:01+0000"/ 
            }
            }
            );
    }

  function changedText() {
   document.getElementById('teks').innerHTML = "Welcome People!";
    }

  const postStatus = () => {

    FB.ui({
    method: 'share',
    href: 'localhost:8000/lab-8/',
      }, function(response){
        populateFacebookUpdates();
      });   

};
